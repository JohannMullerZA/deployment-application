'use strict'

const DeploymentController = require('./deployment-controller')
const Config = require('./config.json')

const executeProcess = (url, azureUrl, azureCredentials, dest) => {
  return new Promise((resolve, reject) => {
    (async () => {
      try {
        await DeploymentController.downloadFile(url, dest, () => {
          console.log('Done')
        })

        await DeploymentController.deployZip(azureUrl, azureCredentials, dest)

        resolve(true)
      } catch (e) {
        reject(e)
      }
    })()
  })
}

// Use dynamic branch name
const URL = `https://${Config.bitbucket.USERNAME}:${Config.bitbucket.PASSWORD}@${Config.bitbucket.URL}/johann.zip`
const AZURE_URL = `${Config.azure.HOST}/${Config.azure.PATH}`
const AZURE_CREDENTIALS = Config.azure.CREDENTIALS

executeProcess(URL, AZURE_URL, AZURE_CREDENTIALS, Config.zip_details.PATH)
