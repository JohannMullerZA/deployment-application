'use strict'

const HTTPS = require('https')
const FS = require('fs')

const downloadFile = async (url, dest, cb) => {
  const file = FS.createWriteStream(dest)
  HTTPS.get(url, (response) => {
    response.pipe(file)
    file.on('finish', () => {
      file.close(cb)
    })
  }).on('error', (err) => {
    FS.unlink(dest)
    if (cb) cb(err.message)
  })
}

const deployZip = async (url, credentials, src) => {
  const zippedFile = FS.createReadStream(src)

  // Refactor to use host and path
  const options = {
    auth: credentials,
    method: 'PUT',
    hostname: url,
    localAddress: false,
    headers: {
      'Content-Type': 'application/zip'
    },
    body: zippedFile
  }

  const req = HTTPS.request(options, (res) => {
    console.log('in request')
    res.on('data', (d) => {
      console.log(d)
    })
  })

  req.on('error', (e) => {
    console.log(e)
  })

  req.end()
}

exports.downloadFile = downloadFile
exports.deployZip = deployZip
